﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace SplineTester
{
    public partial class DrawablePanel : Panel
    {

        private List<MyPoint> _drop;
        private List<MyLine> _tangents;

        public MyPoint myBlinkingPoint = null;
        public List<MyPoint> coloredPoints = null;
        Brush blinkingBrush;
        Brush coloredBrush;

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<MyLine> Tangents
        {
            get
            {
                return _tangents;
            }
            set
            {
                _tangents = value;
                //base.Image = _drop.DropImage.Image;
            }
        }
        
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<MyPoint> Drop
        {
            get
            {
                return _drop;
            }
            set
            {
                _drop = value;
                //base.Image = _drop.DropImage.Image;
            }
        }

        /*[Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Image Image
        {
            get
            {
                return base.Image;
            }
            set
            {
                //base.Image = value;
                _drop = new List<MyPoint>();
            }
        }
        */
        #region Drawing Variables

        #endregion


        #region Point Selection, Dragging Etc, Variables

        private bool _dragging;

        MyPoint selectedPoint = null;
        int selectedPointIndex = -1;

        private bool SelectDrop;

        public Boolean InsertLine;
        public Boolean InsertDrop;

        #endregion

      
        #region Helper Functions

        public void ClearDropPoints()
        {
            _drop.Clear();

        }


        Pen dropPen;
        Pen linePen;



        #endregion
              
        
        #region Visual Editor Helper Properties

        [
        Category("Appearance"),
        Description("Show the Drop Control Points")
        ]
        public Boolean ShowDropPoints
        {
            get;
            set;
        }

        [
        Category("Appearance"),
        Description("Show the Drop")
        ]
        public Boolean ShowDrop
        {
            get;
            set;
        }

        [
        Category("Appearance"),
        Description("Show the Line Control Points")
        ]
        public Boolean ShowLinePoints
        {
            get;
            set;
        }

        [
       Category("Appearance"),
       Description("Show the Line")
       ]
        public Boolean ShowLine
        {
            get;
            set;
        }

        #endregion

        public DrawablePanel()
        {
            InitializeComponent();

            dropPen = new Pen(Color.Red);
            linePen = new Pen(Color.Green);
            blinkingBrush =  new SolidBrush(Color.Violet);
            coloredBrush = new SolidBrush(Color.Green);

            this.MouseClick += new MouseEventHandler(DrawablePanel_MouseClick);
            this.MouseDown += new MouseEventHandler(DrawablePanel_MouseDown);
            this.MouseMove += new MouseEventHandler(DrawablePanel_MouseMove);
            this.MouseUp += new MouseEventHandler(DrawablePanel_MouseUp);
            currentTime = DateTime.Now;
        }
        static DateTime currentTime;

        private void DrawablePanel_MouseClick(object sender, MouseEventArgs e)
        {
            if (!_dragging)
            {
                MyPoint backtrackMousePoint = new MyPoint(e.X,e.Y);
                if (InsertLine || InsertDrop)
                {
                    // Left Mouse Button CLICKED
                    if (e.Button == System.Windows.Forms.MouseButtons.Left)
                    {
                        MyPoint isPresent = MyPoint.EmptyPoint;
                        // Drop Selected
                        if (InsertDrop)
                        {
                            // If the point is not already present
                            foreach (MyPoint pt in (List<MyPoint>)_drop)
                            {
                                if (pt.CheckBounds(backtrackMousePoint))
                                {
                                    isPresent = pt;
                                    break;
                                }
                            }
                            if (isPresent == MyPoint.EmptyPoint)
                            {
                                _drop.Add(backtrackMousePoint);
                            }
                        }
                        
                    }
                    //Right Mouse Button CLICKED
                    else if (e.Button == System.Windows.Forms.MouseButtons.Right)
                    {
                        MyPoint toRemove = null;
                        if (InsertDrop)
                        {
                            foreach (MyPoint pt in (List<MyPoint>)_drop)
                            {
                                if (pt.CheckBounds(backtrackMousePoint))
                                {
                                    toRemove = pt;
                                    break;
                                }
                            }
                            if (toRemove != null)
                            {
                                _drop.Remove(toRemove);
                            }
                        }
                        
                    }
                    //Console.WriteLine("Clicked");
                    this.Invalidate();
                }
            }
        }

        private void DrawablePanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (_drop == null)
                return;
            MyPoint backtrackMousePoint = new MyPoint(e.X,e.Y);

            int selIndex;
            MyPoint selPt;
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (GetPoint(backtrackMousePoint, _drop, out selIndex, out selPt))
                {
                    SelectDrop = true; 
                    selectedPoint = selPt;
                    selectedPointIndex = selIndex;
                    _dragging = true;

                    this.Invalidate();
                }
                else
                {
                    _dragging = false;
                }
                //Console.WriteLine("Down " +_dragging );
            }
        }

        private void DrawablePanel_MouseMove(object sender, MouseEventArgs e)
        {
            MyPoint backtrackMousePoint = new MyPoint(e.X,e.Y);
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (_dragging)
                {
                    if (SelectDrop)
                    {
                        int index = _drop.BinarySearch(selectedPoint);
                        if (index >= 0)
                        {
                            _drop.RemoveAt(index);
                            _drop.Insert(index, backtrackMousePoint);
                        }
                        selectedPoint = backtrackMousePoint;
                    }
                   

                    this.Invalidate();
                    //Console.WriteLine("Moving " + selectedPoint);
                }
            }

        }

        private void DrawablePanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                selectedPoint = null;
                selectedPointIndex = -1;
                SelectDrop = false;
                _dragging = false;
                //Console.WriteLine("Up");
                this.Invalidate();
            }
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (_drop == null)
                return;

            if (_drop != null && _drop.Count() >= 1)
            {
                if (ShowDropPoints)
                {
                    foreach (MyPoint pt in (List<MyPoint>)_drop)
                    {
                        e.Graphics.DrawRectangle(dropPen, pt);
                    }
                }
                if (ShowDrop)
                {
                    if (_drop.Count() > 2)
                    {
                        Point[] DropPlots = ((List<MyPoint>)_drop).Select(obj => (Point)obj).ToArray();
                        e.Graphics.DrawCurve(dropPen, DropPlots, 0.5f);
                    }
                }
                foreach (MyLine line in _tangents)
                {
                    List<MyPoint> endPoints = line.endPointAtDistance(line.StartPoint, 20);
                    e.Graphics.DrawLine(linePen, endPoints[0], endPoints[1]);
                }
                if(myBlinkingPoint != null)
                {
                
                    { 
                        e.Graphics.FillRectangle(blinkingBrush, myBlinkingPoint.BoundingRectangle.X, myBlinkingPoint.BoundingRectangle.Y, myBlinkingPoint.BoundingRectangle.Width, myBlinkingPoint.BoundingRectangle.Height);
                    }
                }
                if (coloredPoints != null)
                {
                    foreach (MyPoint item in coloredPoints)
                    {
                        e.Graphics.FillRectangle(coloredBrush, item.BoundingRectangle.X, item.BoundingRectangle.Y, item.BoundingRectangle.Width, item.BoundingRectangle.Height);

                    }
                }

            }
        }

        
        //ToDo: In the future, do something like Binary search on a _sorted_ checklist to get the selected point
        private bool GetPoint(MyPoint chkPt, List<MyPoint> checkList, out int selectedIndex, out MyPoint selectedPt)
        {
            selectedPt = null;
            selectedIndex = -1;
            bool result = false;

            if (checkList == null || checkList.Count == 0)
                return result;

            int i = 0;
            foreach (MyPoint pt in checkList)
            {
                if (pt.CheckBounds(chkPt))
                {
                    selectedPt = pt;
                    selectedIndex = i;
                    result = true;
                    break;
                }
                i++;
            }

            return result;
        }
    }
}
