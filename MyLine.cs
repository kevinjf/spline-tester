﻿// -----------------------------------------------------------------------
// <copyright file="MyLine.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SplineTester
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MyLine
    {

        public List<MyPoint> Line;

        public enum CalculateAngle { RightToLeft, LeftToRight };
        public CalculateAngle Direction { get; set; }

        public Boolean Locked
        {
            get
            {
                if (Line.Exists(pt => pt.Equals(MyPoint.EmptyPoint)) )
                    return false;
                return true;
            }
        }

        private MyPoint _mid;

        public MyPoint MidPoint
        {
            get
            {
                return _mid;
            }
            private set
            {
                _mid = value;
            }
        }

        private double xDelta;
        private double yDelta;

        public double Length
        {
            get;
            private set;
        }

        double _angle;
        public double Angle
        {
            get
            {
                return _angle;
            }
            private set
            {
                _angle =  ((value % 360) + 360) % 360;
                //Console.WriteLine( _angle);
            }
        }

        public double mSlope
        {
            get;
            private set;
        }

        public double bIntercept
        {
            get;
            private set;
        }


        public MyPoint StartPoint
        {
            get
            {
                return Line.First();
            }
            set
            {
                if (Line.First() == MyPoint.EmptyPoint)
                    Line[0] = value;
                else
                    ReplacePoint(Line.First(), value);

                if (Locked)
                    doCalculation();

                //Console.WriteLine("Start: "+this);
            }
        }
      
        public MyPoint EndPoint
        {
            get
            {
                return Line.Last();
            }
            set
            {
                if (Line.Last() == MyPoint.EmptyPoint)
                    Line[1] = value;
                else
                    ReplacePoint(Line.Last(), value);
                
                if (Locked)
                    doCalculation();
               // Console.WriteLine("End: " + this);

            }
        }

        internal void AddPoint(MyPoint toAdd)
        {

            for (int i=0; i<Line.Count;i++)
            {
                if (Line[i] == MyPoint.EmptyPoint)
                {
                    Line[i] = toAdd;
                    break;
                }                
            }
            if (Locked) doCalculation();
            //Console.WriteLine(this);
            
        }
        
        internal void RemovePoint(MyPoint toRemove)
        {
            Line[Line.FindIndex(pt => pt.Equals(toRemove))] = MyPoint.EmptyPoint;

        }

        internal void ReplacePoint(MyPoint toRemove , MyPoint toReplace)
        {
            //Console.WriteLine("From: "+this);
            if (toRemove == MyPoint.EmptyPoint)
            { 
                throw new Exception("Cannot _REPLACE_ Empty Point..Too RISKY!!");
            }

            int index = Line.FindIndex(pt => pt.Equals(toRemove));
            if(index != -1)
            {
             /*   Line[index].X = toReplace.X;
                Line[index].Y = toReplace.Y;*/
                Line[index] = toReplace;
            }
            if (Locked) doCalculation();
            //Console.WriteLine("To: " + this);
        }

        public MyLine()
        {
            Initialize();
        }

        public void Initialize()
        {
            TangentDirection = AngleDirection.Top2Bottom | AngleDirection.Right2Left;
            Line = new List<MyPoint>();

            //Fill 2 Points
            Line.Add(MyPoint.EmptyPoint);
            Line.Add(MyPoint.EmptyPoint);

            Line.Capacity = 2;
            Direction = CalculateAngle.RightToLeft;
        }

        public MyLine(MyPoint startPoint , MyPoint endPoint)
        {

            Initialize();
            
            StartPoint = startPoint;
            EndPoint = endPoint;
            
        }

        public MyLine(MyPoint startPoint,double slope)
        {

            Initialize();

            StartPoint = startPoint;
            
            mSlope = slope;
            bIntercept = (double)startPoint.Y - (double)slope * (double)startPoint.X;

            EndPoint = endPointAtDistance(startPoint, 25)[0];
            doCalculation();

        }


        // Another Method
        /*public Boolean IsOnLineSegment(int x , int y)
                {
            return (( (y - (mSlope * x) - bIntercept) == 0) ? true : false);
                }*/

        private void doCalculation()
        {
            if (Line.Count < 2)
                return;

            xDelta = EndPoint.X - StartPoint.X;
            yDelta = EndPoint.Y - StartPoint.Y;

            if ((xDelta == 0) && (yDelta == 0))
            {
                throw new ArgumentException("Points are same. NO Line is possible");
            }

            MidPoint = new MyPoint((StartPoint.X + EndPoint.X) / 2, (StartPoint.Y + EndPoint.Y) / 2);

            CalculateSlope();
            CalculateIntercept();
            CalculateLength();
            AngleBetweenLineAndXaxis();

        }

        private void CalculateIntercept()
        {
            mSlope = yDelta / xDelta;
        }

        private void CalculateSlope()
        {
            bIntercept = (StartPoint.Y) - (mSlope * StartPoint.X);
        }

        public double XgivenY(double Y)
        {
            return (Y - bIntercept) / mSlope;
        }

        public double YgivenX(double X)
        {
            return (mSlope * X) + bIntercept;
        }


        public List<MyPoint> endPointAtDistance(MyPoint point , double distance)
        {
            List<MyPoint> endPoints = new List<MyPoint>();

            int x1 = point.X;
            int y1 = point.Y;

            double d = distance;

            double xVal = (d / (Math.Sqrt(Math.Pow(mSlope, 2) + 1)) );
            double yVal = (mSlope * d / (Math.Sqrt(Math.Pow(mSlope, 2) + 1)));

            endPoints.Add(new MyPoint((int)-xVal + x1, (int)-yVal + y1));
            endPoints.Add(new MyPoint((int)xVal + x1, (int)yVal + y1));


            //int x = 0, y = 0;
            
            /*
            double nx = point.X + distance* Math.Cos(Angle);
            double ny = point.Y + distance* Math.Sin(Angle);
            */
            /*
            double d = distance;//this.Length;

            double k = distance / d;

            int x = (int)(this.StartPoint.X + xDelta * distance);
            int y = (int)(this.StartPoint.Y + yDelta * distance);

            MyPoint newPoint = new MyPoint(x, y);

           // System.Diagnostics.Debug.Assert(this.Angle == new MyLine(this.StartPoint,newPoint).Angle);
            return newPoint;


           // return new MyPoint(x, y);
             * */


            return endPoints; 
        }
        

        private void Swap(ref MyPoint StartPoint, ref MyPoint EndPoint)
        {
            MyPoint tmp = StartPoint;
            StartPoint = EndPoint;
            EndPoint = tmp;
        }

        private void CalculateLength()
        {
            Length = MyPoint.DistanceBetween(StartPoint, EndPoint);
        }

        [Flags]
        public enum AngleDirection {

            Top2Bottom = 0x1,
            Bottom2Top = 0x2,
            Right2Left = 0x4,
            Left2Right = 0x8};

        public AngleDirection TangentDirection
        {
            get;
            set;
        }

        private void AngleBetweenLineAndXaxis()
        {
            //double oldAngle = Angle;

            int x,y;


            if ((TangentDirection & AngleDirection.Bottom2Top) == AngleDirection.Bottom2Top)
            {
                y = EndPoint.Y - StartPoint.Y;  
            }
            else// if ((TangentDirection & AngleDirection.Top2Bottom) == AngleDirection.Top2Bottom)
            {
                y = StartPoint.Y - EndPoint.Y;
                
            }
            if ((TangentDirection & AngleDirection.Left2Right) == AngleDirection.Left2Right)
            {
                x = StartPoint.X - EndPoint.X;
                
            }
            else// if ((TangentDirection & AngleDirection.Right2Left) == AngleDirection.Right2Left)
            {
                x = EndPoint.X - StartPoint.X;
                
            }

            Angle = Math.Atan2(y, x) * Rad2Deg;
            /*if(oldAngle!=Angle) 
                Console.WriteLine(" Angle : "+ Angle);*/
        }
        public const double Rad2Deg = 180.0 / Math.PI;
        public static double AngleBetweenLines( MyLine Line1,MyLine Line2)
        {

            double dx1, dy1, dx2, dy2;

            dx1 = Line1.EndPoint.X - Line1.StartPoint.X;
            dy1 = Line1.EndPoint.Y - Line1.StartPoint.Y;
            dx2 = Line2.EndPoint.X - Line2.StartPoint.X;
            dy2 = Line2.EndPoint.Y - Line2.StartPoint.Y;

            double angle = ((dx1 * dx2) + (dy1 * dy2)) / ((Math.Sqrt(dx1 * dx1 + dy1 * dy1)) * (Math.Sqrt(dx2 * dx2 + dy2 * dy2)));

            return Math.Acos(angle) * 180d / Math.PI;
        }

        public double isCloserTo(MyPoint pt3)
        {
            return ((pt3.X - StartPoint.X) * xDelta + (pt3.Y - StartPoint.Y) * yDelta) / (xDelta * xDelta + yDelta * yDelta);
        }



        public double DistanceFromPoint(MyPoint pt3) {

            double u = isCloserTo(pt3);

            MyPoint closestPoint;
            if (u < 0) {
                closestPoint = StartPoint;
            } else if (u > 1) {
                closestPoint = EndPoint;
            } else {
                closestPoint = new MyPoint((int) (StartPoint.X + u * xDelta), (int) (StartPoint.Y + u * yDelta));
            }

            return MyPoint.DistanceBetween(closestPoint,pt3);
        }         


        /// <summary>
        /// Returns the position of the Point with respect to the line
        /// </summary>
        /// <param name="ln">The Line</param>
        /// <param name="pt">The Point</param>
        /// <returns>-1  if Below
        ///                 1  if Above
        ///                 0 if On the line
        ///                 </returns>
        public static int PositionOfPointWRTLine(MyLine ln, MyPoint pt)
        {
            double y = ln.YgivenX(pt.X);
            if (y > pt.Y)
                return -1;
            else if (y < pt.Y)
                return 1;
            return 0;
        }
        
        /// <summary>
        /// Clear the Line / Initialize it
        /// </summary>
        internal void Clear()
        {
            Initialize();
            _mid = MyPoint.EmptyPoint;
            xDelta = 0;
            yDelta = 0;

            Length = 0;
            Angle = 0;
            mSlope =0 ;
        }

        #region Overridden Methods

        /// <summary>
        /// Useful During Debugging
        /// </summary>
        /// <returns>String format of the line</returns>
        public override string ToString()
        {
            StringBuilder b = new StringBuilder();

            foreach (MyPoint pts in Line)
            {
                b.Append("\tX:"+ pts.X + " , Y:"+pts.Y);
            }

            b.Append("\n y = " + this.mSlope + " * x + " + bIntercept + " : Angle= "+ _angle);
            return b.ToString();
        }

        #endregion
    }
}
