﻿// -----------------------------------------------------------------------
// <copyright file="MyPoint.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SplineTester
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Drawing;
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MyPoint  : IComparable
    {


        #region Private Variables

        /// <summary>
        /// The size of the Rectangle[Square], Size * 2 points in width and Height 
        /// </summary>
        private readonly int BOUNDINGBOXSIZE = 2;
        
        private Point _point;

        private int UpperY, LeftX, DownY, RightX;

        #endregion

        #region Public Variables

        public Rectangle BoundingRectangle;

        public Point CenterPoint
        {
            get
            {
                return _point;
            }

            private set
            {
                _point = value;
            }
        }

        public int X
        {
            get
            {
                return this._point.X;
            }
            set
            {
                this._point.X = value;
                UpdatePoint();
            }
        }
        public int Y
        {
            get
            {
                return this._point.Y;
            }
            set
            {
                this._point.Y = value;
                UpdatePoint();
            }
        }
        #endregion


        #region Implicit Conversion Helpers        

        //Implicitly Convert to Rectangle DataType - Useful During Drawing
        public static implicit operator Rectangle(MyPoint pt)
        {
            return pt.BoundingRectangle;
        }

        //Implicitly Convert to Point DataType - Useful During Sorting
        public static implicit operator Point(MyPoint pt)
        {
            return pt.CenterPoint;
        }

        //Implicitly Convert to Point DataType - Useful During Debugging
        public static implicit operator string(MyPoint x)
        {
            return x.ToString();
        }
        
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor - Takes X and Y values
        /// </summary>
        /// <param name="ptX">X value</param>
        /// <param name="ptY">Y value</param>
        public MyPoint(int ptX, int ptY)
        {
            _point = new Point(ptX, ptY);
            UpdatePoint();
        }
        /// <summary>
        /// Takes a System.Drawing.Point
        /// </summary>
        /// <param name="pt">The System.Drawing.Point object</param>
        public MyPoint(Point pt)
            : this(pt.X, pt.Y)
        {

        }
        #endregion

        #region Overrides, Implementations

        public override string ToString()
        {
            return "X=" + X + " Y=" + Y;
        }

        /// <summary>
        /// Compares according to X values -  -1 if  this.X less than newPoint.X
        ///                                                    1 if  this.X greater than newPoint.X
        /// If X values are same, Compares according to the Y value, similar to above pseudo code.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            if (obj is MyPoint)
            {
                MyPoint cp = (MyPoint)obj;
                if (this.X.CompareTo(cp.X) == 0)
                    return this.Y.CompareTo(cp.Y);
                return this.X.CompareTo(cp.X);
            }
            else
            {
                throw new ArgumentException("Object is not a CurvePoint.");
            }
        }

        #endregion

        
        /// <summary>
        /// Calculates The rectangle on Every change in the value of X or Y
        /// </summary>
        private void UpdatePoint()
        {

            LeftX = this.X - BOUNDINGBOXSIZE;
            RightX = this.X + BOUNDINGBOXSIZE;

            UpperY = this.Y - BOUNDINGBOXSIZE;
            DownY = this.Y + BOUNDINGBOXSIZE;

            if (BoundingRectangle == null)
            {
                BoundingRectangle = new System.Drawing.Rectangle(
                    this.X - this.BOUNDINGBOXSIZE, this.Y - this.BOUNDINGBOXSIZE, this.BOUNDINGBOXSIZE * 2, this.BOUNDINGBOXSIZE * 2);
            }
            else
            {
                BoundingRectangle.X = this.X - this.BOUNDINGBOXSIZE;
                BoundingRectangle.Y = this.Y - this.BOUNDINGBOXSIZE;
                BoundingRectangle.Width = this.BOUNDINGBOXSIZE + this.BOUNDINGBOXSIZE;
                BoundingRectangle.Height = this.BOUNDINGBOXSIZE + this.BOUNDINGBOXSIZE;
            }
        }

        /// <summary>
        /// Check if the given point comes in the boundary of this Point
        /// </summary>
        /// <param name="chkPt">The point to check</param>
        /// <returns></returns>
        public bool CheckBounds(Point chkPt)
        {

            if ((chkPt.X >= LeftX && chkPt.X <= RightX) && (chkPt.Y >= UpperY && chkPt.Y <= DownY))
            {
                 return true;
            }
            return false;
        }

        #region Static Methods and Variables

        /// <summary>
        /// An Empty Point [-1,-1]
        /// </summary>
        public static MyPoint EmptyPoint = new MyPoint(-1, -1);

        /// <summary>
        /// Find Distance between two MyPoint Objects
        /// </summary>
        /// <param name="pt1">First Point</param>
        /// <param name="pt2">Second Point</param>
        /// <returns></returns>
        public static double DistanceBetween(MyPoint pt1, MyPoint pt2)
        {
            return Math.Sqrt(((double)(pt2.Y - pt1.Y) * (double)(pt2.Y - pt1.Y)) + ((double)(pt2.X - pt1.X) * (double)(pt2.X - pt1.X)));
        }

        /// <summary>
        /// Find Distance between two Point Objects
        /// </summary>
        /// <param name="pt1">First Point</param>
        /// <param name="pt2">Second Point</param>
        /// <returns></returns>
        public static double DistanceBetween(Point pt1, Point pt2)
        {
            return Math.Sqrt(((double)(pt2.Y - pt1.Y) * (double)(pt2.Y - pt1.Y)) + ((double)(pt2.X - pt1.X) * (double)(pt2.X - pt1.X)));
        }

        #endregion

    }
}
