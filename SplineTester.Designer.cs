﻿namespace SplineTester
{
    partial class SplineTester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.m_3_3 = new System.Windows.Forms.TextBox();
            this.m_3_2 = new System.Windows.Forms.TextBox();
            this.m_3_1 = new System.Windows.Forms.TextBox();
            this.m_3_0 = new System.Windows.Forms.TextBox();
            this.m_2_3 = new System.Windows.Forms.TextBox();
            this.m_2_2 = new System.Windows.Forms.TextBox();
            this.m_2_1 = new System.Windows.Forms.TextBox();
            this.m_2_0 = new System.Windows.Forms.TextBox();
            this.m_1_3 = new System.Windows.Forms.TextBox();
            this.m_1_2 = new System.Windows.Forms.TextBox();
            this.m_1_1 = new System.Windows.Forms.TextBox();
            this.m_1_0 = new System.Windows.Forms.TextBox();
            this.m_0_3 = new System.Windows.Forms.TextBox();
            this.m_0_2 = new System.Windows.Forms.TextBox();
            this.m_0_1 = new System.Windows.Forms.TextBox();
            this.m_0_0 = new System.Windows.Forms.TextBox();
            this.tValueTrackBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.t_minValue = new System.Windows.Forms.TextBox();
            this.t_maxValue = new System.Windows.Forms.TextBox();
            this.t_Value = new System.Windows.Forms.TextBox();
            this.scalarMultiplierNr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.scalarMultiplierDr = new System.Windows.Forms.TextBox();
            this.pointSelector = new System.Windows.Forms.TrackBar();
            this.drawablePanel1 = new SplineTester.DrawablePanel();
            this.clearTangents = new System.Windows.Forms.Button();
            this.clearPoints = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tValueTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointSelector)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(483, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 24);
            this.button1.TabIndex = 1;
            this.button1.Text = "Spline Rebuild";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(165, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "X :  0000  Y = 0000";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(589, 9);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 24);
            this.button2.TabIndex = 3;
            this.button2.Text = "Draw Tangent";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.m_3_3);
            this.groupBox1.Controls.Add(this.m_3_2);
            this.groupBox1.Controls.Add(this.m_3_1);
            this.groupBox1.Controls.Add(this.m_3_0);
            this.groupBox1.Controls.Add(this.m_2_3);
            this.groupBox1.Controls.Add(this.m_2_2);
            this.groupBox1.Controls.Add(this.m_2_1);
            this.groupBox1.Controls.Add(this.m_2_0);
            this.groupBox1.Controls.Add(this.m_1_3);
            this.groupBox1.Controls.Add(this.m_1_2);
            this.groupBox1.Controls.Add(this.m_1_1);
            this.groupBox1.Controls.Add(this.m_1_0);
            this.groupBox1.Controls.Add(this.m_0_3);
            this.groupBox1.Controls.Add(this.m_0_2);
            this.groupBox1.Controls.Add(this.m_0_1);
            this.groupBox1.Controls.Add(this.m_0_0);
            this.groupBox1.Location = new System.Drawing.Point(613, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(178, 166);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basis Matrix";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(50, 137);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "Apply";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // m_3_3
            // 
            this.m_3_3.Location = new System.Drawing.Point(142, 101);
            this.m_3_3.Name = "m_3_3";
            this.m_3_3.Size = new System.Drawing.Size(29, 20);
            this.m_3_3.TabIndex = 15;
            // 
            // m_3_2
            // 
            this.m_3_2.Location = new System.Drawing.Point(95, 101);
            this.m_3_2.Name = "m_3_2";
            this.m_3_2.Size = new System.Drawing.Size(29, 20);
            this.m_3_2.TabIndex = 14;
            // 
            // m_3_1
            // 
            this.m_3_1.Location = new System.Drawing.Point(50, 101);
            this.m_3_1.Name = "m_3_1";
            this.m_3_1.Size = new System.Drawing.Size(29, 20);
            this.m_3_1.TabIndex = 13;
            // 
            // m_3_0
            // 
            this.m_3_0.Location = new System.Drawing.Point(6, 101);
            this.m_3_0.Name = "m_3_0";
            this.m_3_0.Size = new System.Drawing.Size(29, 20);
            this.m_3_0.TabIndex = 12;
            // 
            // m_2_3
            // 
            this.m_2_3.Location = new System.Drawing.Point(142, 75);
            this.m_2_3.Name = "m_2_3";
            this.m_2_3.Size = new System.Drawing.Size(29, 20);
            this.m_2_3.TabIndex = 11;
            // 
            // m_2_2
            // 
            this.m_2_2.Location = new System.Drawing.Point(95, 75);
            this.m_2_2.Name = "m_2_2";
            this.m_2_2.Size = new System.Drawing.Size(29, 20);
            this.m_2_2.TabIndex = 10;
            // 
            // m_2_1
            // 
            this.m_2_1.Location = new System.Drawing.Point(50, 75);
            this.m_2_1.Name = "m_2_1";
            this.m_2_1.Size = new System.Drawing.Size(29, 20);
            this.m_2_1.TabIndex = 9;
            // 
            // m_2_0
            // 
            this.m_2_0.Location = new System.Drawing.Point(6, 75);
            this.m_2_0.Name = "m_2_0";
            this.m_2_0.Size = new System.Drawing.Size(29, 20);
            this.m_2_0.TabIndex = 8;
            // 
            // m_1_3
            // 
            this.m_1_3.Location = new System.Drawing.Point(142, 49);
            this.m_1_3.Name = "m_1_3";
            this.m_1_3.Size = new System.Drawing.Size(29, 20);
            this.m_1_3.TabIndex = 7;
            // 
            // m_1_2
            // 
            this.m_1_2.Location = new System.Drawing.Point(95, 49);
            this.m_1_2.Name = "m_1_2";
            this.m_1_2.Size = new System.Drawing.Size(29, 20);
            this.m_1_2.TabIndex = 6;
            // 
            // m_1_1
            // 
            this.m_1_1.Location = new System.Drawing.Point(50, 49);
            this.m_1_1.Name = "m_1_1";
            this.m_1_1.Size = new System.Drawing.Size(29, 20);
            this.m_1_1.TabIndex = 5;
            // 
            // m_1_0
            // 
            this.m_1_0.Location = new System.Drawing.Point(6, 49);
            this.m_1_0.Name = "m_1_0";
            this.m_1_0.Size = new System.Drawing.Size(29, 20);
            this.m_1_0.TabIndex = 4;
            // 
            // m_0_3
            // 
            this.m_0_3.Location = new System.Drawing.Point(142, 23);
            this.m_0_3.Name = "m_0_3";
            this.m_0_3.Size = new System.Drawing.Size(29, 20);
            this.m_0_3.TabIndex = 3;
            // 
            // m_0_2
            // 
            this.m_0_2.Location = new System.Drawing.Point(95, 23);
            this.m_0_2.Name = "m_0_2";
            this.m_0_2.Size = new System.Drawing.Size(29, 20);
            this.m_0_2.TabIndex = 2;
            // 
            // m_0_1
            // 
            this.m_0_1.Location = new System.Drawing.Point(50, 23);
            this.m_0_1.Name = "m_0_1";
            this.m_0_1.Size = new System.Drawing.Size(29, 20);
            this.m_0_1.TabIndex = 1;
            // 
            // m_0_0
            // 
            this.m_0_0.Location = new System.Drawing.Point(6, 23);
            this.m_0_0.Name = "m_0_0";
            this.m_0_0.Size = new System.Drawing.Size(29, 20);
            this.m_0_0.TabIndex = 0;
            // 
            // tValueTrackBar
            // 
            this.tValueTrackBar.Location = new System.Drawing.Point(509, 245);
            this.tValueTrackBar.Name = "tValueTrackBar";
            this.tValueTrackBar.Size = new System.Drawing.Size(262, 45);
            this.tValueTrackBar.TabIndex = 5;
            this.tValueTrackBar.ValueChanged += new System.EventHandler(this.tValueTrackBar_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(610, 285);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "t =";
            // 
            // t_minValue
            // 
            this.t_minValue.Location = new System.Drawing.Point(472, 245);
            this.t_minValue.Name = "t_minValue";
            this.t_minValue.Size = new System.Drawing.Size(24, 20);
            this.t_minValue.TabIndex = 7;
            this.t_minValue.Text = "0";
            this.t_minValue.TextChanged += new System.EventHandler(this.t_minValue_TextChanged);
            // 
            // t_maxValue
            // 
            this.t_maxValue.Location = new System.Drawing.Point(767, 245);
            this.t_maxValue.Name = "t_maxValue";
            this.t_maxValue.Size = new System.Drawing.Size(24, 20);
            this.t_maxValue.TabIndex = 8;
            this.t_maxValue.Text = "1";
            this.t_maxValue.TextChanged += new System.EventHandler(this.t_maxValue_TextChanged);
            // 
            // t_Value
            // 
            this.t_Value.Location = new System.Drawing.Point(635, 282);
            this.t_Value.Name = "t_Value";
            this.t_Value.Size = new System.Drawing.Size(29, 20);
            this.t_Value.TabIndex = 9;
            this.t_Value.TextChanged += new System.EventHandler(this.t_Value_TextChanged);
            // 
            // scalarMultiplierNr
            // 
            this.scalarMultiplierNr.Location = new System.Drawing.Point(559, 75);
            this.scalarMultiplierNr.Name = "scalarMultiplierNr";
            this.scalarMultiplierNr.Size = new System.Drawing.Size(24, 20);
            this.scalarMultiplierNr.TabIndex = 10;
            this.scalarMultiplierNr.TextChanged += new System.EventHandler(this.scalarMultiplierNr_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(469, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Scalar Multiplier";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(543, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "________";
            // 
            // scalarMultiplierDr
            // 
            this.scalarMultiplierDr.Location = new System.Drawing.Point(559, 117);
            this.scalarMultiplierDr.Name = "scalarMultiplierDr";
            this.scalarMultiplierDr.Size = new System.Drawing.Size(24, 20);
            this.scalarMultiplierDr.TabIndex = 13;
            this.scalarMultiplierDr.TextChanged += new System.EventHandler(this.scalarMultiplierDr_TextChanged);
            // 
            // pointSelector
            // 
            this.pointSelector.Location = new System.Drawing.Point(513, 349);
            this.pointSelector.Name = "pointSelector";
            this.pointSelector.Size = new System.Drawing.Size(258, 45);
            this.pointSelector.TabIndex = 14;
            this.pointSelector.Scroll += new System.EventHandler(this.pointSelector_Scroll);
            // 
            // drawablePanel1
            // 
            this.drawablePanel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.drawablePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.drawablePanel1.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.drawablePanel1.Location = new System.Drawing.Point(12, 26);
            this.drawablePanel1.Name = "drawablePanel1";
            this.drawablePanel1.ShowDrop = true;
            this.drawablePanel1.ShowDropPoints = true;
            this.drawablePanel1.ShowLine = false;
            this.drawablePanel1.ShowLinePoints = false;
            this.drawablePanel1.Size = new System.Drawing.Size(454, 403);
            this.drawablePanel1.TabIndex = 0;
            this.drawablePanel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.drawablePanel1_MouseClick);
            this.drawablePanel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.drawablePanel1_MouseMove);
            // 
            // clearTangents
            // 
            this.clearTangents.Location = new System.Drawing.Point(483, 406);
            this.clearTangents.Name = "clearTangents";
            this.clearTangents.Size = new System.Drawing.Size(87, 23);
            this.clearTangents.TabIndex = 15;
            this.clearTangents.Text = "Clear Tangents";
            this.clearTangents.UseVisualStyleBackColor = true;
            this.clearTangents.Click += new System.EventHandler(this.clearTangents_Click);
            // 
            // clearPoints
            // 
            this.clearPoints.Location = new System.Drawing.Point(576, 406);
            this.clearPoints.Name = "clearPoints";
            this.clearPoints.Size = new System.Drawing.Size(100, 23);
            this.clearPoints.TabIndex = 16;
            this.clearPoints.Text = "Clear Points";
            this.clearPoints.UseVisualStyleBackColor = true;
            this.clearPoints.Click += new System.EventHandler(this.clearPoints_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 441);
            this.Controls.Add(this.clearPoints);
            this.Controls.Add(this.clearTangents);
            this.Controls.Add(this.pointSelector);
            this.Controls.Add(this.scalarMultiplierDr);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.scalarMultiplierNr);
            this.Controls.Add(this.t_Value);
            this.Controls.Add(this.t_maxValue);
            this.Controls.Add(this.t_minValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tValueTrackBar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.drawablePanel1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tValueTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointSelector)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DrawablePanel drawablePanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox m_3_3;
        private System.Windows.Forms.TextBox m_3_2;
        private System.Windows.Forms.TextBox m_3_1;
        private System.Windows.Forms.TextBox m_3_0;
        private System.Windows.Forms.TextBox m_2_3;
        private System.Windows.Forms.TextBox m_2_2;
        private System.Windows.Forms.TextBox m_2_1;
        private System.Windows.Forms.TextBox m_2_0;
        private System.Windows.Forms.TextBox m_1_3;
        private System.Windows.Forms.TextBox m_1_2;
        private System.Windows.Forms.TextBox m_1_1;
        private System.Windows.Forms.TextBox m_1_0;
        private System.Windows.Forms.TextBox m_0_3;
        private System.Windows.Forms.TextBox m_0_2;
        private System.Windows.Forms.TextBox m_0_1;
        private System.Windows.Forms.TextBox m_0_0;
        private System.Windows.Forms.TrackBar tValueTrackBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox t_minValue;
        private System.Windows.Forms.TextBox t_maxValue;
        private System.Windows.Forms.TextBox t_Value;
        private System.Windows.Forms.TextBox scalarMultiplierNr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox scalarMultiplierDr;
        private System.Windows.Forms.TrackBar pointSelector;
        private System.Windows.Forms.Button clearTangents;
        private System.Windows.Forms.Button clearPoints;
    }
}

