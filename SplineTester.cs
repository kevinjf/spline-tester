﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using MathNet.Numerics.LinearAlgebra;
using System.Diagnostics;

namespace SplineTester
{
    public partial class SplineTester : Form
    {
        List<MyLine> tangents;
        public SplineTester()
        {
            InitializeComponent();
            drawablePanel1.Drop = new List<MyPoint>(10);
            drawablePanel1.InsertDrop = true;
            tangents =  new List<MyLine>();
            drawablePanel1.Tangents = tangents;

            CustomInit();

        }

        #region Matrix B

        private double m_0_0_val;
        private double M_0_0
        {
            get
            {
                return m_0_0_val;
            }
            set
            {
                m_0_0_val = value;
                m_0_0.Text = value.ToString();
            }
        }

        private double m_0_1_val;
        private double M_0_1
        {
            get
            {
                return m_0_1_val;
            }
            set
            {
                m_0_1_val = value;
                m_0_1.Text = value.ToString();
            }
        }

        private double m_0_2_val;
        private double M_0_2
        {
            get
            {
                return m_0_2_val;
            }
            set
            {
                m_0_2_val = value;
                m_0_2.Text = value.ToString();
            }
        }

        private double m_0_3_val;
        private double M_0_3
        {
            get
            {
                return m_0_3_val;
            }
            set
            {
                m_0_3_val = value;
                m_0_3.Text = value.ToString();
            }
        }


        private double m_1_0_val;
        private double M_1_0
        {
            get
            {
                return m_1_0_val;
            }
            set
            {
                m_1_0_val = value;
                m_1_0.Text = value.ToString();
            }
        }

        private double m_1_1_val;
        private double M_1_1
        {
            get
            {
                return m_1_1_val;
            }
            set
            {
                m_1_1_val = value;
                m_1_1.Text = value.ToString();
            }
        }

        private double m_1_2_val;
        private double M_1_2
        {
            get
            {
                return m_1_2_val;
            }
            set
            {
                m_1_2_val = value;
                m_1_2.Text = value.ToString();
            }
        }

        private double m_1_3_val;
        private double M_1_3
        {
            get
            {
                return m_1_3_val;
            }
            set
            {
                m_1_3_val = value;
                m_1_3.Text = value.ToString();
            }
        }


        private double m_2_0_val;
        private double M_2_0
        {
            get
            {
                return m_2_0_val;
            }
            set
            {
                m_2_0_val = value;
                m_2_0.Text = value.ToString();
            }
        }

        private double m_2_1_val;
        private double M_2_1
        {
            get
            {
                return m_2_1_val;
            }
            set
            {
                m_2_1_val = value;
                m_2_1.Text = value.ToString();
            }
        }

        private double m_2_2_val;
        private double M_2_2
        {
            get
            {
                return m_2_2_val;
            }
            set
            {
                m_2_2_val = value;
                m_2_2.Text = value.ToString();
            }
        }

        private double m_2_3_val;
        private double M_2_3
        {
            get
            {
                return m_2_3_val;
            }
            set
            {
                m_2_3_val = value;
                m_2_3.Text = value.ToString();
            }
        }


        private double m_3_0_val;
        private double M_3_0
        {
            get
            {
                return m_3_0_val;
            }
            set
            {
                m_3_0_val = value;
                m_3_0.Text = value.ToString();
            }
        }

        private double m_3_1_val;
        private double M_3_1
        {
            get
            {
                return m_3_1_val;
            }
            set
            {
                m_3_1_val = value;
                m_3_1.Text = value.ToString();
            }
        }

        private double m_3_2_val;
        private double M_3_2
        {
            get
            {
                return m_3_2_val;
            }
            set
            {
                m_3_2_val = value;
                m_3_2.Text = value.ToString();
            }
        }

        private double m_3_3_val;
        private double M_3_3
        {
            get
            {
                return m_3_3_val;
            }
            set
            {
                m_3_3_val = value;
                m_3_3.Text = value.ToString();
            }
        }


        private void m_0_0_TextChanged(object sender, EventArgs e)
        {

            double newVal = 0; if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_0_0 != newVal)
                M_0_0 = newVal;
        }
        private void m_0_1_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0; if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_0_1 != newVal)
                M_0_1 = newVal;
        }
        private void m_0_2_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_0_2 != newVal)
                M_0_2 = newVal;
        }
        private void m_0_3_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_0_3 != newVal)
                M_0_3 = newVal;
        }

        private void m_1_0_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_1_0 != newVal)
                M_1_0 = newVal;
        }
        private void m_1_1_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_1_1 != newVal)
                M_1_1 = newVal;
        }
        private void m_1_2_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_1_2 != newVal)
                M_1_2 = newVal;
        }
        private void m_1_3_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_1_3 != newVal)
                M_1_3 = newVal;
        }
        private void m_2_0_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_2_0 != newVal)
                M_2_0 = newVal;
        }
        private void m_2_1_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_2_1 != newVal)
                M_2_1 = newVal;
        }
        private void m_2_2_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_2_2 != newVal)
                M_2_2 = newVal;
        }
        private void m_2_3_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_2_3 != newVal)
                M_2_3 = newVal;
        }
        private void m_3_0_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_3_0 != newVal)
                M_3_0 = newVal;
        }
        private void m_3_1_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_3_1 != newVal)
                M_3_1 = newVal;
        }
        private void m_3_2_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_3_2 != newVal)
                M_3_2 = newVal;
        }
        private void m_3_3_TextChanged(object sender, EventArgs e)
        {
            double newVal = 0;            if (double.TryParse((((TextBox)sender).Text), out newVal))
            if (M_3_3 != newVal)
                M_3_3 = newVal;
        }



        private void CustomInit()
        {
            M_0_0 = -1;
            M_1_0 = 3;
            M_2_0 = -3;
            M_3_0 = 1;

            M_0_1 = 3;
            M_1_1 = -6;
            M_2_1 = -0;
            M_3_1 = 4;

            M_0_2 = -3;
            M_1_2 = 3;
            M_2_2 = 3;
            M_3_2 = 1;

            M_0_3 = 1;
            M_1_3 = 0;
            M_2_3 = 0;
            M_3_3 = 0;




            this.m_0_0.TextChanged += new System.EventHandler(this.m_0_0_TextChanged);
            this.m_0_1.TextChanged += new System.EventHandler(this.m_0_1_TextChanged);
            this.m_0_2.TextChanged += new System.EventHandler(this.m_0_2_TextChanged);
            this.m_0_3.TextChanged += new System.EventHandler(this.m_0_3_TextChanged);

            this.m_1_0.TextChanged += new System.EventHandler(this.m_1_0_TextChanged);
            this.m_1_1.TextChanged += new System.EventHandler(this.m_1_1_TextChanged);
            this.m_1_2.TextChanged += new System.EventHandler(this.m_1_2_TextChanged);
            this.m_1_3.TextChanged += new System.EventHandler(this.m_1_3_TextChanged);

            this.m_2_0.TextChanged += new System.EventHandler(this.m_2_0_TextChanged);
            this.m_2_1.TextChanged += new System.EventHandler(this.m_2_1_TextChanged);
            this.m_2_2.TextChanged += new System.EventHandler(this.m_2_2_TextChanged);
            this.m_2_3.TextChanged += new System.EventHandler(this.m_2_3_TextChanged);

            this.m_3_0.TextChanged += new System.EventHandler(this.m_3_0_TextChanged);
            this.m_3_1.TextChanged += new System.EventHandler(this.m_3_1_TextChanged);
            this.m_3_2.TextChanged += new System.EventHandler(this.m_3_2_TextChanged);
            this.m_3_3.TextChanged += new System.EventHandler(this.m_3_3_TextChanged);



            scalarMultiplierNr.Text = "1";
            scalarMultiplierDr.Text = "6";

            t_minValue.Text = (-1).ToString();
            t_maxValue.Text = (2).ToString();
            t_Value.Text =    (0).ToString();

        }



        #endregion

        Matrix bMatrix, tMatrix, PtMatrix;
        //List<MyPoint> Points;
        MyPoint prev, cur, next, next2;

        private double tVal
        {
            get;
            set;
        }

        double scalarMultiplierValue = 0;

        public void VariableSetter()
        {

            setBMatrix();

            

            if (drawablePanel1.Drop == null)
                drawablePanel1.Drop = new List<MyPoint>();

            if (drawablePanel1.Drop.Count < 3) return;            


            // get points (P-1,P,P+1,P+2) in the original snake
            RefreshPointCount();

            prev = drawablePanel1.Drop[(pointSelector.Value + drawablePanel1.Drop.Count - 1) % drawablePanel1.Drop.Count];
            cur = drawablePanel1.Drop[(pointSelector.Value + drawablePanel1.Drop.Count) % drawablePanel1.Drop.Count];
            next = drawablePanel1.Drop[(pointSelector.Value + drawablePanel1.Drop.Count + 1) % drawablePanel1.Drop.Count];
            next2 = drawablePanel1.Drop[(pointSelector.Value + drawablePanel1.Drop.Count + 2) % drawablePanel1.Drop.Count];



            setPtMatrix(prev, cur, next, next2);


            /*
            Console.WriteLine("TMatrix: " );
            Console.WriteLine(tMatrix.ToString());

            Console.WriteLine("BMatrix: ");
            Console.WriteLine(bMatrix.ToString());

            Console.WriteLine("PtMatrix: ");
            Console.WriteLine(PtMatrix.ToString());
            */

        }

        private void setBMatrix()
        {

            if (bMatrix == null)
                bMatrix = new Matrix(4, 4);

            bMatrix[0, 0] = M_0_0;
            bMatrix[0, 1] = M_0_1;
            bMatrix[0, 2] = M_0_2;
            bMatrix[0, 3] = M_0_3;

            bMatrix[1, 0] = M_1_0;
            bMatrix[1, 1] = M_1_1;
            bMatrix[1, 2] = M_1_2;
            bMatrix[1, 3] = M_1_3;

            bMatrix[2, 0] = M_2_0;
            bMatrix[2, 1] = M_2_1;
            bMatrix[2, 2] = M_2_2;
            bMatrix[2, 3] = M_2_3;

            bMatrix[3, 0] = M_3_0;
            bMatrix[3, 1] = M_3_1;
            bMatrix[3, 2] = M_3_2;
            bMatrix[3, 3] = M_3_3;

            //Console.WriteLine(bMatrix.ToString());

            bMatrix.Multiply(scalarMultiplierValue);
        }

        private void setTMatrix(double t)
        {
            if (tMatrix == null)
                tMatrix = new Matrix(1, 4);
            tMatrix[0, 3] = 1;
            tMatrix[0, 2] = t;
            tMatrix[0, 1] = tMatrix[0, 2] * t;
            tMatrix[0, 0] = tMatrix[0, 1] * t;
        }

        private void setTangentTMatrix(double t)
        {
            if (tMatrix == null)
                tMatrix = new Matrix(1, 4);
            tMatrix[0, 3] = 0;
            tMatrix[0, 2] = 1;
            tMatrix[0, 1] = 2 * t;
            tMatrix[0, 0] = 3 * t * t;
        }

        private void setPtMatrix(MyPoint prev, MyPoint cur, MyPoint next, MyPoint next2)
        {

            if (PtMatrix == null)
                PtMatrix = new Matrix(4, 2);

            PtMatrix[0, 0] = prev.X;
            PtMatrix[1, 0] = cur.X;
            PtMatrix[2, 0] = next.X;
            PtMatrix[3, 0] = next2.X;

            PtMatrix[0, 1] = prev.Y;
            PtMatrix[1, 1] = cur.Y;
            PtMatrix[2, 1] = next.Y;
            PtMatrix[3, 1] = next2.Y;


            if (drawablePanel1.coloredPoints == null)
            {
                drawablePanel1.coloredPoints = new List<MyPoint>();
            }
            else
            {
                drawablePanel1.coloredPoints.Clear();
            }


            drawablePanel1.coloredPoints.Add(prev);
            drawablePanel1.coloredPoints.Add(cur);
            drawablePanel1.coloredPoints.Add(next);
            drawablePanel1.coloredPoints.Add(next2);

        }


        public void RefreshPointCount()
        {
            pointSelector.Minimum = 0;
            pointSelector.Maximum = drawablePanel1.Drop.Count;
        }


        public void DrawPoint()
        {


            setTMatrix(tVal);

            if (tMatrix == null || bMatrix == null || prev == null || cur == null || next == null || next2 == null)
                return;
            
            Matrix finalValueMatrix = tMatrix.Multiply(bMatrix).Multiply(PtMatrix);
            MyPoint toDraw = new MyPoint((int) (0.5+finalValueMatrix[0, 0]) , (int) (0.5+ finalValueMatrix[0, 1]));


            //Console.WriteLine(toDraw);

            drawablePanel1.myBlinkingPoint = toDraw;
            SplineTangents(toDraw);

        }
        

        private void SplineTangents(MyPoint tangentPoint)
        {

            VariableSetter();


            setTangentTMatrix(tVal);

            Matrix finalValueMatrix = tMatrix.Multiply(bMatrix).Multiply(PtMatrix);


            if (tangents == null)
            {
                tangents = new List<MyLine>();
            }
            else
            {
                tangents.Clear();
            }

            MyPoint StartPoint = tangentPoint == null ? cur : tangentPoint;



            MyLine tangent = new MyLine(StartPoint, ((double)finalValueMatrix[0, 1] / (double)finalValueMatrix[0, 0]));


            //Console.WriteLine(finalValueMatrix[0, 0] + " - " + finalValueMatrix[0, 1]);
            //Console.WriteLine (" && " + (finalValueMatrix[0, 0] / finalValueMatrix[0, 1]));


            tangents.Add(tangent);

            
        }



        public void SplineRebuild()
        {

            VariableSetter();

            const int space = 25;

            List<MyPoint> pts = new List<MyPoint>();
            List<MyPoint> ds = drawablePanel1.Drop;

            if (ds.Count < 3) return;
            int n = 1;
            foreach (MyPoint pt in ds)
            {
                pts.Add(pt);
                //if (n == 1 || n == ds.Count) pts.Add(pt);
                n++;
            }

            
            // precompute length(i) = length of the snake from start to point #i
            double[] clength = new double[pts.Count];
            clength[0] = 0;
            for (int i = 0; i < pts.Count - 1; i++)
            {
                Point cur = pts[i];
                Point next = pts[(i + 1)];// % pts.Count];

                clength[i + 1] = clength[i] + MyPoint.DistanceBetween(cur, next);
            }


            // compute number of points in the new snake
            double total = clength[pts.Count - 1];
            int nmb = (int)(0.5 + total / space);

            // build a new snake

            List<MyPoint> newsnake = new List<MyPoint>(nmb);
            newsnake.Add(ds.First());
            
            for (int i = 0, j = 1; j < nmb; j++)
            {
                // current length in the new snake

                double dist = (j * total) / nmb;


                // find corresponding interval of points in the original snake

                while (!(clength[i] <= dist && dist < clength[i + 1])) i++;

                // get points (P-1,P,P+1,P+2) in the original snake
                MyPoint prev, cur, next, next2;
                int prevIndex, curIndex, nextIndex, next2Index;

                if (i - 1 < 0)
                    prevIndex = i;
                else
                    prevIndex = i - 1;

                curIndex = i;

                if (i + 2 >= pts.Count)
                {
                    if (i + 1 >= pts.Count)
                    {
                        nextIndex = curIndex;
                        next2Index = curIndex;
                    }
                    else
                    {
                        nextIndex = i + 1;
                        next2Index = nextIndex;
                    }
                }
                else
                {
                    nextIndex = i + 1;
                    next2Index = i + 2;
                }


                prev = pts[prevIndex]; //+ pts.Count - 1 % pts.Count];
                cur = pts[curIndex];
                next = pts[nextIndex];// % pts.Count];
                next2 = pts[next2Index];// % pts.Count];


                // do cubic spline interpolation
                double t = 0;
                if (dist != 0)
                    t = (dist - clength[curIndex]) / (clength[nextIndex] - clength[curIndex]);

                setTMatrix(t);
                
                Matrix NewMatrix = tMatrix.Multiply(bMatrix);
                //    NewMatrix.Multiply((double) 1 / (double) 6);
                //NewMatrix.Multiply(scalarMultiplierValue);

                setPtMatrix(prev, cur, next, next2);


                Matrix FinalMatrix = NewMatrix.Multiply(PtMatrix);

               
                //Debug.Assert((int)(0.5 + FinalMatrix[0, 0]) == newpoint.X);
                //Debug.Assert((int)(0.5 + FinalMatrix[0, 1]) == newpoint.Y);

                Point newpoint = new MyPoint((int)(0.5 + FinalMatrix[0, 0]), (int)(0.5 + FinalMatrix[0, 1]));

                // add computed point to the new snake
                newsnake.Add(new MyPoint((int)(0.5 + FinalMatrix[0, 0]), (int)(0.5 + FinalMatrix[0, 1])));
            }

            newsnake.Add(ds.Last());

            ds.Clear();


            foreach (MyPoint pt in newsnake)
            {
                ds.Add(pt);
            }


        }


        private void StraightLineTangents()
        {

            if (drawablePanel1.Drop.Count > 3)
            {
                MyLine LeftTangent = new MyLine();
                LeftTangent.TangentDirection = MyLine.AngleDirection.Top2Bottom | MyLine.AngleDirection.Right2Left;
                LeftTangent.StartPoint = drawablePanel1.Drop[0];
                int i = 0;
                for (i = 0; drawablePanel1.Drop[i].X == drawablePanel1.Drop[0].X && drawablePanel1.Drop[i].Y == drawablePanel1.Drop[0].Y; i++) ;
                LeftTangent.EndPoint = drawablePanel1.Drop[i];
                LeftTangent.EndPoint = LeftTangent.endPointAtDistance(LeftTangent.StartPoint, 8)[1];

                MyLine RightTangent = new MyLine();
                RightTangent.TangentDirection = MyLine.AngleDirection.Top2Bottom | MyLine.AngleDirection.Left2Right;
                RightTangent.StartPoint = drawablePanel1.Drop[drawablePanel1.Drop.Count - 1];
                for (i = 0; drawablePanel1.Drop[drawablePanel1.Drop.Count - 1 - i].X == drawablePanel1.Drop[drawablePanel1.Drop.Count - 1].X && drawablePanel1.Drop[drawablePanel1.Drop.Count - 1 - i].Y == drawablePanel1.Drop[drawablePanel1.Drop.Count - 1].Y; i++) ;
                RightTangent.EndPoint = drawablePanel1.Drop[drawablePanel1.Drop.Count - 1 - i];
                RightTangent.EndPoint = RightTangent.endPointAtDistance(RightTangent.StartPoint, 8)[1];
                Console.WriteLine("Left CA = " + LeftTangent.Angle + "\t\t\t" + "Right CA = " + RightTangent.Angle);
                tangents.Add(LeftTangent);
                tangents.Add(RightTangent);
            }


        }





        #region Events

        private void tValueTrackBar_ValueChanged(object sender, EventArgs e)
        {
            tVal =((double)((TrackBar)sender).Value / (double)100);

            //Console.WriteLine("TrackBar Value::" + ((TrackBar)sender).Value + " & tValue "+tVal);



            if(!t_Value.Text.Equals(tVal.ToString()))
            {
                t_Value.Text = tVal.ToString();
            }





            //DoCalc?
            VariableSetter();


            DrawPoint();
            drawablePanel1.Invalidate();


        }

        private void t_minValue_TextChanged(object sender, EventArgs e)
        {
            int val = 0;
            if (int.TryParse((((TextBox)sender).Text),out val))
                 tValueTrackBar.Minimum = val * 100;

        }

        private void t_maxValue_TextChanged(object sender, EventArgs e)
        {
            int val = 0;
            if (int.TryParse((((TextBox)sender).Text), out val))
                tValueTrackBar.Maximum = val * 100;

        }

        private void t_Value_TextChanged(object sender, EventArgs e)
        {
            double val = double.Parse(((TextBox)sender).Text) * 100;
            if (tValueTrackBar.Value != val)
                tValueTrackBar.Value = (int)val ;


        }


        private void scalarMultiplierNr_TextChanged(object sender, EventArgs e)
        {
            calculateScalarValue();
           
        }


        private void scalarMultiplierDr_TextChanged(object sender, EventArgs e)
        {

            calculateScalarValue();
        }

        private void drawablePanel1_MouseClick(object sender, MouseEventArgs e)
        {
            RefreshPointCount();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            VariableSetter();
        }

        private void clearTangents_Click(object sender, EventArgs e)
        {
            if (tangents != null)
                tangents.Clear();
            else
                tangents = new List<MyLine>();
            drawablePanel1.Invalidate();
        }

        private void clearPoints_Click(object sender, EventArgs e)
        {
            if (drawablePanel1.Drop != null)
                drawablePanel1.Drop.Clear();
            else
                drawablePanel1.Drop = new List<MyPoint>();


            if (drawablePanel1.coloredPoints == null)
            {
                drawablePanel1.coloredPoints = new List<MyPoint>();
            }
            else
            {
                drawablePanel1.coloredPoints.Clear();
            }
            drawablePanel1.myBlinkingPoint = null;

            clearTangents_Click(this, e);
        }

        private void pointSelector_Scroll(object sender, EventArgs e)
        {
            prev = drawablePanel1.Drop[(pointSelector.Value + drawablePanel1.Drop.Count - 1) % drawablePanel1.Drop.Count];
            cur = drawablePanel1.Drop[(pointSelector.Value + drawablePanel1.Drop.Count) % drawablePanel1.Drop.Count];
            next = drawablePanel1.Drop[(pointSelector.Value + drawablePanel1.Drop.Count + 1) % drawablePanel1.Drop.Count];
            next2 = drawablePanel1.Drop[(pointSelector.Value + drawablePanel1.Drop.Count + 2) % drawablePanel1.Drop.Count];

            if (drawablePanel1.coloredPoints == null)
            {
                drawablePanel1.coloredPoints = new List<MyPoint>();
            }
            else
            {
                drawablePanel1.coloredPoints.Clear();
            }
            drawablePanel1.coloredPoints.Add(prev);
            drawablePanel1.coloredPoints.Add(cur);
            drawablePanel1.coloredPoints.Add(next);
            drawablePanel1.coloredPoints.Add(next2);

            drawablePanel1.Invalidate();

        }


        private void button1_Click(object sender, EventArgs e)
        {
            SplineRebuild();
            drawablePanel1.Invalidate();
        }

        private void drawablePanel1_MouseMove(object sender, MouseEventArgs me)
        {

            this.label1.Text = "X =  " + me.X + " Y=  " + me.Y + "  ";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SplineTangents(null);
            drawablePanel1.Invalidate();
        }


        #endregion
        
        private void calculateScalarValue()
        {
            int Nrval; int Drval;
            if (int.TryParse((((TextBox)scalarMultiplierNr).Text), out Nrval))
                if (int.TryParse((((TextBox)scalarMultiplierDr).Text), out Drval))
                    scalarMultiplierValue = (double)Nrval / (double)Drval;
        }


    }
}
